import './ListDate.css'
import {useContext, useRef} from "react"


function ListDate(props) {


    // const myDate = ['01', '01', '01', '02', '03', '03', '04', '05', '06', '06', '06']
    const myDate = ['1401/01/12', '1401/01/12', '1401/01/12', '1401/01/13', '1401/01/14', '1401/01/14', '1401/01/14', '1401/01/15', '1401/01/16', '1401/01/16', '1401/01/16','1401/01/17']

    let color1
    return (
        <div className={"main"}>
            {myDate.map((item, index, array) => {
                if(array[index]===array[index+1]||array[index]===array[index-1]){
                    color1='blue'
                }else {
                    color1='red'
                }

                return (
                    <div className={'t1'}>
                        <div className={color1} >
                            {item}
                        </div>
                    </div>

                );

            })}
        </div>
    )


}


export default ListDate;